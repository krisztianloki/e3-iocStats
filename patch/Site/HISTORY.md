# remove_iocname_from_desc.p0.patch

The .template file ioc.template potentially has too long a description field if the variable IOCNAME is too long. There is no real need to keep IOCNAME as part of the description, so we have removed it.

* created by Simon Rose (simon.rose@ess.eu)
* Wednesday, September 8, 2021
